import React from 'react';
import { BrowserRouter,Switch, Route,Redirect,Link } from 'react-router-dom';
import universal from 'react-universal-component'



const login = universal(props=> import('./components/login'))
const list = universal(props=> import('./components/list'))
const homePage = universal(props=> import('./components/form'))







function App() {
return (
    <div className="App">
      
    
    <BrowserRouter>
    <Switch>
    <Route exact path='/' component={login} />
    <Route exact path='/home-page' component={homePage}/>
    <Route exact path='/news-list' component={list} />
   
    </Switch>
    </BrowserRouter>

      
    </div>
  );
}

export default App;
