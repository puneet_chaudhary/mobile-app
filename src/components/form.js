import React, { Component } from "react";
import { Col } from "react-materialize";
import "./scss/form.scss";

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        f_name: "",
        l_name: "",
        gender: "male",
        dob: ""
      },
      error: {
        f_name: "",
        l_name: "",
        dob: ""
      },
      rules: {
        f_name: {
          required: true,
          type: "text"
        },
        l_name: {
          required: true,
          type: "text"
        },
        dob: {
          required: true,
          type: "date"
        }
      },
      finalSubmit: false
    };
  }

  handleChange = e => {
    let data = this.state.data;
    let error = this.state.error;
    let rule = this.state.rules;
    let name = e.target.name;
    let value = e.target.value;

    if (name == "dob") {
      value = this.addhyphen(value);
    }
    data[name] = value;
    function isFilled(value) {
      if (value != null && value != "") return true;
      else return false;
    }

    function validType(value, type) {
      var dataType = {
        text: /^[a-zA-Z ]+$/
      };
      if (value.match(dataType[type])) return true;
      else return false;
    }

    if (rule[name].required == true) {
      if (!isFilled(data[name])) {
        error[name] = "*required";
      } else if (!validType(data[name], rule[name].type)) {
        error[name] = "*not valid";
      } else {
        error[name] = "";
      }
    } else if (isFilled(data[name])) {
      if (!validType(data[name], rule[name].type)) {
        error[name] = "*not valid";
      } else {
        error[name] = "";
      }
    }
    var count = 0;
    Object.keys(this.state.rules).forEach(function(element) {
      if (
        (error[element] != "" || error[element] != null) &&
        (data[element] == "" || data[element] == null) &&
          rule[element].required == true
      ) {
        count++;
      }
    });
    if (count == 0) {
      data.validate = true;
    } else {
      data.validate = false;
    }

    this.setState({ data, error }, () => {});
  };

  addhyphen = value => {
    function checkValue(str, max) {
      if (str.charAt(0) !== "0" || str == "00") {
        var num = parseInt(str);
        if (isNaN(num) || num <= 0 || num > max) num = 1;
        str =
          num > parseInt(max.toString().charAt(0)) && num.toString().length == 1
            ? "0" + num
            : num.toString();
      }
      return str;
    }

    var input = value;
    if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
    var values = input.split("/").map(function(v) {
      return v.replace(/\D/g, "");
    });
    if (values[0]) values[0] = checkValue(values[0], 31);
    if (values[1]) values[1] = checkValue(values[1], 12);
    var output = values.map(function(v, i) {
      return v.length == 2 && i < 2 ? v + " / " : v;
    });
    return output.join("").substr(0, 14);
  };

  handleGender = type => {
    let data = this.state.data;
    data["gender"] = type;
    this.setState({ data }, () => {});
  };

  handleSubmit = () => {
    var data = this.state.data;
    let error = this.state.error;
    let rules = this.state.rules;
    let finalSubmit = this.state.finalSubmit;
    function validate() {
      let valid = true;
      Object.keys(rules).forEach(element => {
        if (error[element].length > 0) {
          valid = false;
        }
      });
      return valid;
    }

    if (validate() && finalSubmit) {
      setTimeout(() => {
        window.location.href = "/news-list";
      }, 200);
    } else {
      Object.keys(error).forEach(function(element) {
        if (
          (error[element] == "" || error[element] == null) &&
          (data[element] == "" || data[element] == null) &&
            rules[element].required == true
        ) {
          error[element] = "*Required";
        }
      });
      this.setState({ error, finalSubmit: true }, () => {});
    }
  };

  render() {
    let data = this.state.data;
    let error = this.state.error;

    return (
      <div>
        <div className="col s12 m10 offset-m1 no-pad">
          <Col s={12} m={6} className="lr-nopad no-pad">
            <div className="top">What's Your name?</div>

            <div className="lr-nopad signin-top">
              <input
                className={error.f_name.length > 1 ? "red-text" : "plain-text"}
                type="text"
                maxLength="50"
                name="f_name"
                s={12}
                value={data.f_name}
                placeholder="First Name"
                onChange={this.handleChange}
              />
            </div>
            <div className="lr-nopad password">
              <input
                type="text"
                className={error.l_name.length > 1 ? "red-text" : "plain-text"}
                name="l_name"
                s={12}
                placeholder="Last Name"
                value={data.l_name}
                onChange={this.handleChange}
              />
            </div>

            <div className="gender">
              <div className="gender-top">And Your Gender?</div>
              <div
                className={data.gender == "male" ? "male" : "female"}
                onClick={e => this.handleGender("male")}
              >
                Male
              </div>
              <div className="set-female">
                <div
                  className={data.gender == "female" ? "male" : "female"}
                  onClick={e => this.handleGender("female")}
                >
                  Female
                </div>
              </div>
            </div>

            <div className="date-top">What's your date of birth?</div>
            <input
              type="text"
              className={error.dob.length > 1 ? "red-text" : "plain-text"}
              name="dob"
              s={12}
              placeholder="DD/MM/YYYY"
              value={data.dob}
              onChange={this.handleChange}
            />
          </Col>
        </div>

        <div className="forward-arrow" onClick={this.handleSubmit}>
          >
        </div>
      </div>
    );
  }
}
