import React, { Component } from "react";
import axios from "axios";
import "./scss/list.scss";

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiData: []
    };
  }

  getApiData = () => {
    axios
      .get(
        "http://newsapi.org/v2/top-headlines?" +
          "country=in&" +
          "apiKey=e7203f7400b54832b47716f1ec86d184"
      )
      .then(res => {
        if (res) {
          this.setState({ apiData: res.data.articles }, () => {});
        }
      })
      .catch(err => {
        console.log("err", err);
      });
  };

  componentDidMount() {
    this.getApiData();
  }

  render() {
    let apiData = this.state.apiData;

    return (
      <div className="top">
        {apiData &&
          apiData.map((item, index) => {
            return (
              <div className="outer-side">
                <img src={item.urlToImage} />

                <div className="author">{item.author}</div>
                <div className="date">{item.publishedAt}</div>
                <div className="desc">{item.title}</div>
              </div>
            );
          })}
      </div>
    );
  }
}
