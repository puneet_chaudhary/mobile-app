import React, { Component } from "react";
import { Row, Col } from "react-materialize";
import "./scss/login.scss";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        username: "",
        password: ""
      },
      error: {
        username: "",
        password: ""
      },
      userType: /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/,
      isLoggin: false
    };
  }

  handleChange = e => {
    let data = this.state.data;
    let error = this.state.error;
    let userType = this.state.userType;
    let value = e.target.value;
    let name = e.target.name;
    data[name] = value;
    this.setState({ data });
    if (name == "username") {
      if (userType.test(value)) {
        error.username = "";
      } else {
        error.username = "*Invalid";
      }
    }

    this.setState({ error });
  };

  handleLogin = () => {
    let username = "qwerty@gmail.com";
    let password = "qwerty@321";
    let data = this.state.data;
    if (data.username == username && data.password == password) {
      this.setState({ isLoggin: true }, () => {
        if (this.state.isLoggin) {
          setTimeout(() => {
            window.location.href = "/home-page";
          }, 200);
        } else {
          window.location.href = "/";
        }
      });
    } else if (data.username == "" && data.password == "") {
      alert("Please Enter Fields");
    } else {
      alert("Invalid user");
    }
  };

  componentDidMount() {}

  render() {
    let data = this.state.data;
    let error = this.state.error;
    return (
      <div>
        <div className="m-height">
          <div className="login-signup-heading">
            <h1 className="center-bold">PWA</h1>
            <h3 className="tag-line">Chatchphrase</h3>
          </div>
          <div className="col s12 m10 offset-m1 no-pad">
            <Col s={12} m={6} className="lr-nopad no-pad">
              <div className="lr-nopad signin-top">
                <input
                  type="text"
                  className="login"
                  maxLength="50"
                  name="username"
                  s={12}
                  value={data.username}
                  placeholder="LoginId"
                  autoFocus
                  onChange={this.handleChange}
                />
              </div>
              <div className="lr-nopad password">
                <input
                  type="password"
                  className="password-text"
                  name="password"
                  s={12}
                  placeholder="Password"
                  onChange={this.handleChange}
                />
              </div>
              <Row>
                <div className="offset-s3">
                  <a
                    className="sign-in"
                    style={{ cursor: "pointer" }}
                    onClick={this.handleLogin}
                  >
                    LogIn
                  </a>
                </div>
              </Row>
            </Col>
          </div>
        </div>
      </div>
    );
  }
}
